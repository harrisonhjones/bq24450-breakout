# BQ24450 Breakout
A simple breakout board for a BQ24450.

![Soldered IC with Header Pins](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/Banner.png "Soldered IC with Header Pin")

##Motivation
I've been looking for a way to charge a small lead acid battery (think alarm system backup battery) so that I can create a "drop in" battery module for powering other (largely stationary given the weight of lead-acid batteries) projects of mine. I recently found the BQ24450 from Texas Instruments so I spun up a breakout so I could test the IC on a breadboad.

##BQ24450
The BQ24450 is a *"Integrated Charge Controller for Lead-Acid Batteries"* - Texas Instruments. It is a small IC which senses battery voltage and charge current and uses an external "valve" transistor to charge 2-4 cell Lead-Acid Batteries. The datasheet for the BQ24450 can be found in the reference folder or [here](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/Reference/bq24450.pdf "BQ24450 Datasheet")

##Design
The breakout was designed using EAGLE 6.5. The schematic and board files are in the /PCB Design folder. The PCB design was quite simple, just breakout each pin to a breadboard compatible pin layout. 0.1" headers were chosen and spaced far enough apart that the board would fit on a breadboard. The PCB design was also an excercise in saving space. Given the fixed header pitch and the breadboard constraints the only way to save space was to make the board as thin as possible. In order to do so the BQ24450 was turned on its side given that there was plenty of space down the length of the board. The resulting copper routing ended up being a little stange looking but in the end the resulting board was pretty small.

###Schematic
![picture of the schematic](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Design/Schematic.png "Schematic")

The schematic can be found in the PCB Design folder or [here](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Design/Version1.sch "BQ24450 Breakout Schematic")

###Board

####Overview
![picture of the schematic](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Design/PCB-Total.png "Overview of the PCB")

The schematic can be found in the PCB Design folder or [here](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Design/Version1.brd "BQ24450 Breakout Board")

####Top
![picture of the schematic](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Design/PCB-Top.png "Top of the PCB")

####Bottom
![picture of the schematic](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Design/PCB-Bot.png "Bottom of the PCB")

##BOM

* BQ24450 - Sample from Texas Instruments
* 0.1" headers - had on-hand. 
* PCB - Ordered from OSH Park. Can be re-ordered [here](https://oshpark.com/shared_projects/CWTqYVLL "OSH Park Shared Project"). 

##Assembly
Assembling the breakout was fairly easy. I decided to go the route of using solder paste and hotair instead of a fine-tip soldering iron for soldering the BQ24450's SMD package. This was due mostly to the fact that I had solder paste and didn't have a fine-tip soldering iron. 

###Instructions

* Place solder paste on the board

![PCB with Solder Paste Applied](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Assembly/PCBSolderPaste.jpg "PCB with Solder Paste Applied")

* Place the BQ24450 on the solder paste in the correct location and orientation

![PCB with IC on Solder Paste](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Assembly/PCBSolderPasteIC.jpg "PCB with IC on Solder Paste")

* Using a hot air gun slowly apply heat until the solder paste flows and all pads are soldered

![Soldered IC without Header Pins](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Assembly/PCBSolderedNoPins.jpg "Soldered IC without Header Pins")

* Using a soldering iron solder on the 0.1" headers

![Soldered IC with Header Pins](https://bytebucket.org/harrisonhjones/bq24450-breakout/raw/master/PCB%20Assembly/CompletedBreakoutWithPins.jpg "Soldered IC with Header Pin")